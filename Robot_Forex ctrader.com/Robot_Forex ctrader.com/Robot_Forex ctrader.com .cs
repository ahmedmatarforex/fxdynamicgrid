﻿//https://ctrader.com/algos/cbots/show/225
using System;
using cAlgo.API;

namespace cAlgo.Robots
{
    [Robot(TimeZone = TimeZones.UTC, AccessRights = AccessRights.FullAccess)]
    public class RobotForex : Robot
    {
        [Parameter(DefaultValue = 100000, MinValue = 1000)]
        public int FirstLot { get; set; }

        [Parameter("Take_Profit", DefaultValue = 180, MinValue = 10)]
        public int TakeProfit { get; set; }

        [Parameter("Tral_Start", DefaultValue = 50)]
        public int Tral_Start { get; set; }

        [Parameter("Tral_Stop", DefaultValue = 50)]
        public int Tral_Stop { get; set; }

        [Parameter(DefaultValue = 300)]
        public int PipStep { get; set; }

        [Parameter(DefaultValue = 5, MinValue = 2)]
        public int MaxOrders { get; set; }

        private Position position;
        private bool RobotStopped;
        private int LotStep = 10000;

        protected override void OnStart()
        {
            Positions.Opened += Positions_Opened;
        }

        protected override void OnStop()
        {
            for (int i = 0; i < Positions.Count; i++)
            {
                Positions[i].Close();
            }
        }
        protected override void OnTick()
        {
            double Bid = Symbol.Bid;
            double Ask = Symbol.Ask;
            double Point = Symbol.TickSize;

            if (Trade.IsExecuting)
                return;
            if (Positions.Count > 0 && RobotStopped)
                return;
            else
                RobotStopped = false;

            if (Positions.Count == 0)
                SendFirstOrder(FirstLot);
            else
                ControlSeries();

            // adjust trailing stop
            foreach (var position in Positions)
            {
                if (position.TradeType == TradeType.Buy)
                {
                    if (Bid - GetAveragePrice(TradeType.Buy) >= Tral_Start * Point)
                    {
                        if (Bid - Tral_Stop * Point >= position.StopLoss)
                        {
                            Print(1, position.StopLoss);
                            ModifyPosition(position, Bid - Tral_Stop * Point, position.TakeProfit);
                            Print(2, position.StopLoss);
                            this.Stop();
                        }

                    }
                }

                if (position.TradeType == TradeType.Sell)
                {
                    if (GetAveragePrice(TradeType.Sell) - Ask >= Tral_Start * Point)
                    {
                        if (Ask + Tral_Stop * Point <= position.StopLoss || position.StopLoss == 0)
                        {
                            Print(3, position.StopLoss);
                            ModifyPosition(position, Ask + Tral_Stop * Point, position.TakeProfit);
                            Print(4, position.StopLoss);
                            this.Stop();
                        }
                    }
                }
            }

        }

        protected override void OnError(Error CodeOfError)
        {
            if (CodeOfError.Code == ErrorCode.NoMoney)
            {
                RobotStopped = true;
                Print("ERROR!!! No money for order open, robot is stopped!");
            }
            else if (CodeOfError.Code == ErrorCode.BadVolume)
            {
                RobotStopped = true;
                Print("ERROR!!! Bad volume for order open, robot is stopped!");
            }
        }

        private void SendFirstOrder(int OrderVolume)
        {
            int Signal = GetStdIlanSignal();
            if (Signal >= 0)
                switch (Signal)
                {
                    case 0:
                        ExecuteMarketOrder(TradeType.Buy, Symbol.Name, OrderVolume);
                        break;
                    case 1:
                        ExecuteMarketOrder(TradeType.Sell, Symbol.Name, OrderVolume);
                        break;
                }
        }

        protected void Positions_Opened(PositionOpenedEventArgs args)
        {
            Position openedPosition = args.Position;
            double? StopLossPrice = null;
            double? TakeProfitPrice = null;

            if (Positions.Count == 1)
            {
                position = openedPosition;
                if (position.TradeType == TradeType.Buy)
                    TakeProfitPrice = position.EntryPrice + TakeProfit * Symbol.TickSize;
                if (position.TradeType == TradeType.Sell)
                    TakeProfitPrice = position.EntryPrice - TakeProfit * Symbol.TickSize;
            }
            else
                switch (GetPositionsSide())
                {
                    case 0:
                        TakeProfitPrice = GetAveragePrice(TradeType.Buy) + TakeProfit * Symbol.TickSize;
                        break;
                    case 1:
                        TakeProfitPrice = GetAveragePrice(TradeType.Sell) - TakeProfit * Symbol.TickSize;
                        break;
                }

            for (int i = 0; i < Positions.Count; i++)
            {
                position = Positions[i];
                if (StopLossPrice != null || TakeProfitPrice != null)
                    ModifyPosition(position, position.StopLoss, TakeProfitPrice);
            }
        }

        private double GetAveragePrice(TradeType TypeOfTrade)
        {
            double Result = Symbol.Bid;
            double AveragePrice = 0;
            long Count = 0;

            for (int i = 0; i < Positions.Count; i++)
            {
                position = Positions[i];
                if (position.TradeType == TypeOfTrade)
                {
                    AveragePrice += position.EntryPrice * position.Volume;
                    Count += position.Volume;
                }
            }
            if (AveragePrice > 0 && Count > 0)
                Result = AveragePrice / Count;
            return Result;
        }

        private int GetPositionsSide()
        {
            int Result = -1;
            int i, BuySide = 0, SellSide = 0;

            for (i = 0; i < Positions.Count; i++)
            {
                if (Positions[i].TradeType == TradeType.Buy)
                    BuySide++;
                if (Positions[i].TradeType == TradeType.Sell)
                    SellSide++;
            }
            if (BuySide == Positions.Count)
                Result = 0;
            if (SellSide == Positions.Count)
                Result = 1;
            return Result;
        }

        private void ControlSeries()
        {
            int _pipstep, NewVolume, Rem;
            int BarCount = 25;
            int Del = MaxOrders - 1;

            if (PipStep == 0)
                _pipstep = GetDynamicPipstep(BarCount, Del);
            else
                _pipstep = PipStep;

            if (Positions.Count < MaxOrders)
                switch (GetPositionsSide())
                {
                    case 0:
                        if (Symbol.Ask < FindLastPrice(TradeType.Buy) - _pipstep * Symbol.TickSize)
                        {
                            NewVolume = Math.DivRem((int)(FirstLot + FirstLot * Positions.Count), LotStep, out Rem) * LotStep;
                            if (!(NewVolume < LotStep))
                                ExecuteMarketOrder(TradeType.Buy, Symbol.Name, NewVolume);
                        }
                        break;
                    case 1:
                        if (Symbol.Bid > FindLastPrice(TradeType.Sell) + _pipstep * Symbol.TickSize)
                        {
                            NewVolume = Math.DivRem((int)(FirstLot + FirstLot * Positions.Count), LotStep, out Rem) * LotStep;
                            if (!(NewVolume < LotStep))
                                ExecuteMarketOrder(TradeType.Sell, Symbol.Name, NewVolume);
                        }
                        break;
                }
        }

        private int GetDynamicPipstep(int CountOfBars, int Del)
        {
            int Result;
            double HighestPrice = 0, LowestPrice = 0;
            int StartBar = Bars.ClosePrices.Count - 2 - CountOfBars;
            int EndBar = Bars.ClosePrices.Count - 2;

            for (int i = StartBar; i < EndBar; i++)
            {
                if (HighestPrice == 0 && LowestPrice == 0)
                {
                    HighestPrice = Bars.HighPrices[i];
                    LowestPrice = Bars.LowPrices[i];
                    continue;
                }
                if (Bars.HighPrices[i] > HighestPrice)
                    HighestPrice = Bars.HighPrices[i];
                if (Bars.LowPrices[i] < LowestPrice)
                    LowestPrice = Bars.LowPrices[i];
            }
            Result = (int)((HighestPrice - LowestPrice) / Symbol.TickSize / Del);
            return Result;
        }

        private double FindLastPrice(TradeType TypeOfTrade)
        {
            double LastPrice = 0;

            for (int i = 0; i < Positions.Count; i++)
            {
                position = Positions[i];
                if (TypeOfTrade == TradeType.Buy)
                    if (position.TradeType == TypeOfTrade)
                    {
                        if (LastPrice == 0)
                        {
                            LastPrice = position.EntryPrice;
                            continue;
                        }
                        if (position.EntryPrice < LastPrice)
                            LastPrice = position.EntryPrice;
                    }
                if (TypeOfTrade == TradeType.Sell)
                    if (position.TradeType == TypeOfTrade)
                    {
                        if (LastPrice == 0)
                        {
                            LastPrice = position.EntryPrice;
                            continue;
                        }
                        if (position.EntryPrice > LastPrice)
                            LastPrice = position.EntryPrice;
                    }
            }
            return LastPrice;
        }

        private int GetStdIlanSignal()
        {
            int Result = -1;
            int LastBarIndex = Bars.ClosePrices.Count - 2;
            int PrevBarIndex = LastBarIndex - 1;

            if (Bars.ClosePrices[LastBarIndex] > Bars.OpenPrices[LastBarIndex])
                if (Bars.ClosePrices[PrevBarIndex] > Bars.OpenPrices[PrevBarIndex])
                    Result = 0;
            if (Bars.ClosePrices[LastBarIndex] < Bars.OpenPrices[LastBarIndex])
                if (Bars.ClosePrices[PrevBarIndex] < Bars.OpenPrices[PrevBarIndex])
                    Result = 1;
            return Result;
        }
    }
}

/*
FirstLot, TakeProfit = 180, Tral_Start, Tral_Stop, PipStep = 300, MaxOrders, LotStep = 10000

tick: 
	point = 0.00001
	no positions => open first (openFirst(firstLot))
	else controlSeries()
	
	=> adjust all stop losses // => could not check it for now
	
		
		
openFirst(firstLot):
	check signal:
		signal 0 : buy for firstLot
		signal 1 : sell for firstLot

ControlSeries:
	if losing and pips < -pipStep, open a new position for last volume + lotStep
		
when a new position opened:
	adjust take profit for all positions
	take profit for all = average price + take profit
	
average price:
	(all entry prices * their volume) / volume
	
FindLastPrice:
	get the lowest for buy or highest for sell entry prices
GetDynamicPipstep:
	highest price for last 25 bars - lowest price for same / number of max trades count

*/