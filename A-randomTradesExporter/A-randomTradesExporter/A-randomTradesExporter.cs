using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using cAlgo.API;
using cAlgo.API.Collections;
using cAlgo.API.Indicators;
using cAlgo.API.Internals;


namespace cAlgo.Robots
{
    [Robot(AccessRights = AccessRights.FullAccess)]
    public class ArandomTradesExporter : Robot
    {
        [Parameter(DefaultValue = 10)]
        public int TakeProfitParam { get; set; }
        [Parameter(DefaultValue = 15)]
        public int StopLossParam { get; set; }
        [Parameter(DefaultValue = 1000)]
        public int LotSize { get; set; }
        [Parameter(DefaultValue = 5)]
        public int MaxTradeTime { get; set; }
        
        readonly static string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
        readonly string filePath = System.IO.Path.Combine(desktopPath, "RandomTrades.txt");
        public Random rnd = new Random();
        private System.Collections.Generic.IDictionary<int,TradeData> dictionary = new Dictionary<int,TradeData>();

        public RelativeStrengthIndex rsi;
        public StochasticOscillator stoch;
        public MacdCrossOver macd;
        public DirectionalMovementSystem dmi;
        public WilliamsPctR william;
        public CommodityChannelIndex ccind;

        
        protected override void OnStart()
        {
            System.IO.File.WriteAllText(filePath,string.Empty);
            Positions.Closed += OnPositionClosed;
            rsi = Indicators.RelativeStrengthIndex(Bars.OpenPrices, 14);
            stoch = Indicators.StochasticOscillator(9,6,6,MovingAverageType.Exponential);
            macd = Indicators.MacdCrossOver(26, 12, 14);
            dmi = Indicators.DirectionalMovementSystem(14);
            william = Indicators.WilliamsPctR(14);
            ccind = Indicators.CommodityChannelIndex(14);
            
        }

        protected override void OnBar(){
            // only take 1 of each 10 trades as startrs
            if(rnd.Next(0,100) > 10){
            
                if(rnd.Next(0,100) > 50){
                    makeSell();
                }
                else{
                    makeBuy();
                }
            }
        }
        
        protected void OnPositionClosed(PositionClosedEventArgs obj){
            getPositionClosedDataAndWriteFile(obj.Position);
        }

        void makeSell(){
            TradeResult tr = ExecuteMarketOrder(TradeType.Sell,Symbol.Name,LotSize,"",StopLossParam,TakeProfitParam);
            // buy is 1, sell is -1
            TradeData td = new TradeData(-1,rsi,stoch,macd,dmi,william,ccind,Bars);
            dictionary.Add(tr.Position.Id,td);
        }
        void makeBuy(){
            TradeResult tr = ExecuteMarketOrder(TradeType.Buy,Symbol.Name,LotSize,"",StopLossParam,TakeProfitParam);
            // buy is 1, sell is -1
            TradeData td = new TradeData(1,rsi,stoch,macd,dmi,william,ccind,Bars);
            dictionary.Add(tr.Position.Id,td);
        }
        void getPositionClosedDataAndWriteFile(Position p){
            TradeData td = null;
            dictionary.TryGetValue(p.Id,out td);
            dictionary.Remove(p.Id);
            if(td == null) return;
            
            // if the server took more than MaxTradeTime hours skip the trade, and if it hit the stop loss it should be 
            // the opposit side, otherwise the trade is good
            if(p.EntryTime.AddHours(MaxTradeTime) > Server.Time){
                td.direction = 0;
            }
            else if(p.NetProfit < 0 ){
                td.direction = -1 * td.direction;
            }
            System.IO.File.AppendAllLines(filePath,new[] {td.getDataObject()+","});
            
        }
        
        
    }
}


class TradeData{
    public string startMonth;
    public string startDay;
    public string startHour;
    public string startDayOfWeek ;
    
    public string marketData;
    
    public string rsi;
    public string stoch_PercentD;
    public string stoch_PercentK;
    public string macd_MACD;
    public string macd_Signal;
    public string macd_Histogram;
    public string dmi_ADX;
    public string dmi_DIMinus;
    public string dmi_DIPlus;
    public string william;
    public string ccind;
    
    public int direction;

    
    
    public TradeData(int direction, RelativeStrengthIndex rsi,
    StochasticOscillator stoch,MacdCrossOver macd,DirectionalMovementSystem dmi,
    WilliamsPctR william,CommodityChannelIndex cci, Bars bars){
        // we need to generate an array for the past 20 bars on each of the above parameters
        this.rsi = getPast20Barsof(rsi.Result, "rsi");
        this.stoch_PercentD = getPast20Barsof(stoch.PercentD, "stoch_PercentD");
        this.stoch_PercentK = getPast20Barsof(stoch.PercentK, "stoch_PercentK");
        this.macd_Histogram = getPast20Barsof(macd.Histogram, "macd_Histogram");
        this.macd_MACD = getPast20Barsof(macd.MACD, "macd_MACD");
        this.macd_Signal = getPast20Barsof(macd.Signal, "macd_Signal");
        this.dmi_ADX = getPast20Barsof(dmi.ADX, "dmi_ADX");
        this.dmi_DIMinus = getPast20Barsof(dmi.DIMinus, "dmi_DIMinus");
        this.dmi_DIPlus = getPast20Barsof(dmi.DIPlus, "dmi_DIPlus");
        this.william = getPast20Barsof(william.Result, "william");
        this.ccind = getPast20Barsof(cci.Result, "cci");
                
        this.marketData = getMarketDataSeries(bars);
        DateTime openTime = bars.Last().OpenTime;
        this.startMonth = openTime.Month.ToString();
        this.startDay = openTime.Day.ToString();
        this.startDayOfWeek = ((int)openTime.DayOfWeek).ToString();
        this.startHour = openTime.Hour.ToString();
        this.direction = direction;
        
    }
    
    public string getPast20Barsof(IndicatorDataSeries result, string name){
        string last = "";
        for(int i = 0 ; i < 20 ; i++){
            last += name+i + ":" + Math.Round(result.Last(i), 3) + ",";
        }
        return last;
    }
 
    public string getMarketDataSeries(Bars bars){
        string last = "";
        for(int i = 0 ; i < 20 ; i++){
            last += "OpenPrices"+i + ":" + Math.Round(bars.OpenPrices.Last(i), 5) + ",";
            last += "ClosePrices"+i + ":" + Math.Round(bars.ClosePrices.Last(i), 5) + ",";
            last += "HighPrices"+i + ":" + Math.Round(bars.HighPrices.Last(i), 5) + ",";
            last += "LowPrices"+i + ":" + Math.Round(bars.LowPrices.Last(i), 5) + ",";
        }
        return last;
    }
    
    public string getDataObject(){
     //   return direction + " ";
        return @$"{{ 
    direction: {direction},
    startMonth:{startMonth},
    startDay:{startDay},
    startHour:{startHour},
    startDayOfWeek:{startDayOfWeek},
    {rsi}
    {stoch_PercentD}
    {stoch_PercentK}
    {macd_MACD}
    {macd_Signal}
    {macd_Histogram}
    {dmi_ADX}
    {dmi_DIMinus}
    {dmi_DIPlus}
    {william}
    {ccind}
    {marketData}
    }}";
    
    }
}