﻿using System;
using System.Collections.Generic;
using System.Linq;
using cAlgo.API;
using cAlgo.API.Indicators;
using cAlgo.API.Internals;
using cAlgo.Indicators;


namespace cAlgo.Robots
{
    [Robot(TimeZone = TimeZones.UTC, AccessRights = AccessRights.None)]
    public class AccumulativeGrid : Robot
    {
        [Parameter("Pip Size", DefaultValue = 10, MinValue = 1)]
        public int PipSize { get; set; }

        [Parameter("Volume", DefaultValue = 5000, MinValue = 1000)]
        public int Volume { get; set; }

        public List<Position> buyList;
        public List<Position> sellList;

        public double realisedProfit;
        public double sharedProfit;

        double nextLevelBuy, nextLevelSell;

        Position lowestBuy, lowestSell, highestBuy, highestSell;

        public bool closeTag;

        protected override void OnStart()
        {
            // Put your initialization logic here
            Positions.Opened += onPositionOpened;
            Positions.Closed += onPositionClosed;
            closeTag = false;
            init();
        }

        protected override void OnTick()
        {
            // Put your core logic here

            if (Symbol.Spread > (3 * Symbol.PipSize))
                return;
            //Print(1);
            openPositions();
            //Print(2);
            closePosition();
            //Print(3);
            setSellLoop();
            //Print(4);
            setBuyLoop();
            //Print(5);
            if (buyList.Count == 0)
            {
                ExecuteMarketOrder(TradeType.Buy, Symbol.Name, Volume);
            }

            if (sellList.Count == 0)
            {
                ExecuteMarketOrder(TradeType.Sell, Symbol.Name, Volume);
            }


        }

        protected override void OnStop()
        {
            // Put your deinitialization logic here
            closeTag = true;
            while (Positions.Count > 0)
            {
                Positions[0].Close();
            }
        }

        public void init()
        {
            buyList = new List<Position>();
            sellList = new List<Position>();

            realisedProfit = 0;
            sharedProfit = 0;

            nextLevelSell = Symbol.Bid;
            nextLevelBuy = Symbol.Ask;
            ExecuteMarketOrder(TradeType.Buy, Symbol.Name, Volume);
            ExecuteMarketOrder(TradeType.Sell, Symbol.Name, Volume);

        }

        protected void onPositionOpened(PositionOpenedEventArgs obj)
        {
            if (obj.Position.TradeType == TradeType.Buy)
            {
                buyList.Add(obj.Position);
                setBuyLoop();
            }
            else
            {
                sellList.Add(obj.Position);

                setSellLoop();
            }
        }

        protected void onPositionClosed(PositionClosedEventArgs obj)
        {
            if (closeTag)
                return;

            if (obj.Position.TradeType == TradeType.Buy)
            {
                buyList.Remove(obj.Position);
                lowestBuy = null;

                highestBuy = null;

                setBuyLoop();
            }
            else
            {
                sellList.Remove(obj.Position);

                lowestSell = null;

                highestSell = null;

                setSellLoop();
            }
        }

        public void openPositions()
        {
            if (highestBuy == null || lowestBuy == null || Symbol.Ask >= highestBuy.EntryPrice + (PipSize * Symbol.PipSize) || Symbol.Bid <= lowestBuy.EntryPrice - (PipSize * Symbol.PipSize))
            {
                ExecuteMarketOrder(TradeType.Buy, Symbol.Name, Volume);
            }

            if (highestSell == null || lowestSell == null || Symbol.Ask >= highestSell.EntryPrice + (PipSize * Symbol.PipSize) || Symbol.Bid <= lowestSell.EntryPrice - (PipSize * Symbol.PipSize))
            {
                ExecuteMarketOrder(TradeType.Sell, Symbol.Name, Volume);
            }
        }

        public void closePosition()
        {
            if (highestBuy != null && lowestBuy != null && Symbol.Ask <= highestBuy.EntryPrice - (PipSize * Symbol.PipSize) && Symbol.Bid >= lowestBuy.EntryPrice + (PipSize * Symbol.PipSize))
            {
                double positive = 0, negative = 0;
                for (int i = 0; i < buyList.Count; i++)
                {
                    Position current = buyList[i];
                    if (current.NetProfit > 0)
                        positive += current.NetProfit;

                    if (current.NetProfit < 0)
                        negative += current.NetProfit;
                }


                if (Math.Abs(positive) > Math.Abs(negative) && lowestBuy.Pips > PipSize)
                {
                    // close all negative + lowest buy
                    double calcProfit = negative + lowestBuy.NetProfit;
                    if (calcProfit + sharedProfit > 0)
                    {
                        if (calcProfit > 0)
                        {
                            sharedProfit += calcProfit / 2;
                            realisedProfit += calcProfit / 2;
                        }
                        else
                        {
                            sharedProfit += calcProfit;
                        }

                        closeTag = true;
                        for (int i = 0; i < buyList.Count; i++)
                        {
                            Position current = buyList[i];
                            if (current.NetProfit < 0)
                                current.Close();
                        }
                        closeTag = false;
                        TradeResult tr = lowestBuy.Close();
                        if (tr.Error != null)
                            Print(tr.Error);

                    }
                }
                else if (Math.Abs(negative) > Math.Abs(positive) && highestBuy.Pips <= -PipSize)
                {
                    // close all positives + highest buy
                    double calcProfit = positive + highestBuy.NetProfit;
                    if (calcProfit + sharedProfit > 0)
                    {
                        if (calcProfit > 0)
                        {
                            sharedProfit += calcProfit / 2;
                            realisedProfit += calcProfit / 2;
                        }
                        else
                        {
                            sharedProfit += calcProfit;
                        }

                        closeTag = true;
                        for (int i = 0; i < buyList.Count; i++)
                        {
                            Position current = buyList[i];
                            if (current.NetProfit > 0)
                                current.Close();
                        }
                        closeTag = false;
                        TradeResult tr = highestBuy.Close();
                        if (tr.Error != null)
                            Print(tr.Error);

                    }
                }


            }

            if (highestSell != null && lowestSell != null && Symbol.Ask <= highestSell.EntryPrice - (PipSize * Symbol.PipSize) && Symbol.Bid >= lowestSell.EntryPrice + (PipSize * Symbol.PipSize))
            {
                double positive = 0, negative = 0;
                for (int i = 0; i < sellList.Count; i++)
                {
                    Position current = sellList[i];
                    if (current.NetProfit > 0)
                        positive += current.NetProfit;

                    if (current.NetProfit < 0)
                        negative += current.NetProfit;
                }


                if (Math.Abs(positive) > Math.Abs(negative) && highestSell.Pips > PipSize)
                {
                    // close all negative + highest sell
                    double calcProfit = negative + highestSell.NetProfit;
                    if (calcProfit + sharedProfit > 0)
                    {
                        if (calcProfit > 0)
                        {
                            sharedProfit += calcProfit / 2;
                            realisedProfit += calcProfit / 2;
                        }
                        else
                        {
                            sharedProfit += calcProfit;
                        }

                        closeTag = true;
                        for (int i = 0; i < sellList.Count; i++)
                        {
                            Position current = sellList[i];
                            if (current.NetProfit < 0)
                                current.Close();
                        }
                        closeTag = false;
                        TradeResult tr = highestSell.Close();
                        if (tr.Error != null)
                            Print(tr.Error);

                    }
                }
                else if (Math.Abs(negative) > Math.Abs(positive) && highestBuy.Pips <= -PipSize)
                {
                    // close all positives + lowest buy
                    double calcProfit = positive + lowestSell.NetProfit;
                    if (calcProfit + sharedProfit > 0)
                    {
                        if (calcProfit > 0)
                        {
                            sharedProfit += calcProfit / 2;
                            realisedProfit += calcProfit / 2;
                        }
                        else
                        {
                            sharedProfit += calcProfit;
                        }

                        closeTag = true;
                        for (int i = 0; i < sellList.Count; i++)
                        {
                            Position current = sellList[i];
                            if (current.NetProfit > 0)
                                current.Close();
                        }
                        closeTag = false;
                        TradeResult tr = lowestSell.Close();
                        if (tr.Error != null)
                            Print(tr.Error);

                    }
                }
            }
        }

        public void setBuyLoop()
        {
            for (int i = 0; i < buyList.Count; i++)
            {
                if (lowestBuy == null || lowestBuy.EntryPrice > buyList[i].EntryPrice)
                    lowestBuy = buyList[i];

                if (highestBuy == null || highestBuy.EntryPrice < buyList[i].EntryPrice)
                    highestBuy = buyList[i];
            }
        }
        public void setSellLoop()
        {
            for (int i = 0; i < sellList.Count; i++)
            {
                if (lowestSell == null || lowestSell.EntryPrice > sellList[i].EntryPrice)
                    lowestSell = sellList[i];

                if (highestSell == null || highestSell.EntryPrice < sellList[i].EntryPrice)
                    highestSell = sellList[i];
            }
        }
    }
}
