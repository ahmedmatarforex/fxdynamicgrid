﻿using System;
using System.Linq;
using cAlgo.API;
using cAlgo.API.Indicators;
using cAlgo.API.Internals;
using cAlgo.Indicators;

namespace cAlgo.Robots
{
    [Robot(TimeZone = TimeZones.UTC, AccessRights = AccessRights.None)]
    public class NewcBot : Robot
    {
        [Parameter(DefaultValue = 0.0)]
        public double Parameter { get; set; }

        RelativeStrengthIndex rsi;
        MovingAverage ma;

        protected override void OnStart()
        {
            // Put your initialization logic here
            rsi = Indicators.RelativeStrengthIndex(Bars.ClosePrices, 10);
            ma = Indicators.MovingAverage(Bars.ClosePrices, 200, MovingAverageType.Simple);
        }

        protected override void OnTick()
        {
            // Put your core logic here
        }

        protected override void OnStop()
        {
            // Put your deinitialization logic here
        }

        protected override void OnBar()
        {
            if (Positions.Count == 0)
            {
                // check open criteria
                Print("{0} -- {1}", ma.Result.LastValue.ToString(), rsi.Result.LastValue);
                if (Symbol.Ask > ma.Result.LastValue && Symbol.Bid > ma.Result.LastValue && rsi.Result.LastValue < 30)
                {
                    Print("open");
                    ExecuteMarketOrder(TradeType.Buy, Symbol.Name, 1);
                }

            }
            else
            {
                // check close criteria
                Print("{0} -- {1} == {2}", rsi.Result.LastValue, Positions[0].EntryTime.AddDays(10), Server.Time);
                if (rsi.Result.LastValue > 40 || Positions[0].EntryTime.AddDays(10) >= Server.Time)
                {
                    Print("close");
                    Positions[0].Close();
                }
            }
        }
    }
}
