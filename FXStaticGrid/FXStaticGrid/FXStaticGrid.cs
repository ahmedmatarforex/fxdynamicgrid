﻿using System;
using System.Collections.Generic;
using System.Linq;
using cAlgo.API;
using cAlgo.API.Indicators;
using cAlgo.API.Internals;
using cAlgo.Indicators;

namespace cAlgo.Robots
{
    [Robot(TimeZone = TimeZones.UTC, AccessRights = AccessRights.None)]
    public class FXStaticGrid : Robot
    {

        [Parameter("Pip step", DefaultValue = 10, MinValue = 1)]
        public int PipSize { get; set; }

        [Parameter("Volume", DefaultValue = 5000, MinValue = 1000)]
        public int Volume { get; set; }

        double totalTakenPips;
        bool gridRunning;
        protected override void OnStart()
        {
            totalTakenPips = 0;
            gridRunning = false;
            Positions.Closed += positionClosed;
        }

        protected override void OnTick()
        {
            if (gridRunning == false)
            {
                buildGrid();
            }
            else
            {
                if (totalTakenPips + getGridPips() >= 50)
                {
                    closeGrid();
                }
            }
        }

        protected override void OnStop()
        {
            while (Positions.Count > 0)
            {
                Positions[0].Close();
            }
        }

        protected void positionClosed(PositionClosedEventArgs obj)
        {
            if (!gridRunning)
                return;
            if (obj.Reason == PositionCloseReason.TakeProfit)
            {
                totalTakenPips += obj.Position.Pips;
                TradeResult tr = PlaceLimitOrder(obj.Position.TradeType, Symbol.Name, obj.Position.VolumeInUnits, obj.Position.EntryPrice, "", 0, PipSize);
                if (tr.Error != null)
                    Print(tr.Error);
            }

        }

        public void buildGrid()
        {
            ExecuteMarketOrder(TradeType.Buy, Symbol.Name, Volume, "", 0, PipSize);
            ExecuteMarketOrder(TradeType.Sell, Symbol.Name, Volume, "", 0, PipSize);
            for (int i = 1; i < 200; i++)
            {
                // buy up = sell down = stop
                // buy down = sell up = limit
                TradeResult tr = PlaceLimitOrder(TradeType.Sell, Symbol.Name, Volume, Symbol.Ask + (Symbol.PipSize * i * PipSize), "", 0, PipSize);

                tr = PlaceStopLimitOrder(TradeType.Sell, Symbol.Name, Volume, Symbol.Ask - (Symbol.PipSize * i * PipSize), 0, "", 0, PipSize);

                tr = PlaceStopLimitOrder(TradeType.Buy, Symbol.Name, Volume, Symbol.Bid + (Symbol.PipSize * i * PipSize), 0, "", 0, PipSize);

                tr = PlaceLimitOrder(TradeType.Buy, Symbol.Name, Volume, Symbol.Bid - (Symbol.PipSize * i * PipSize), "", 0, PipSize);

            }

            gridRunning = true;
        }

        public double getGridPips()
        {
            double pips = 0;
            for (int i = 0; i < Positions.Count; i++)
            {
                pips += Positions[i].Pips;
            }
            return pips;
        }

        public void closeGrid()
        {
            gridRunning = false;
            totalTakenPips = 0;
            while (Positions.Count > 0)
            {
                Positions[0].Close();
            }
            while (PendingOrders.Count > 0)
            {
                PendingOrders[0].Cancel();
            }
        }
    }




}
