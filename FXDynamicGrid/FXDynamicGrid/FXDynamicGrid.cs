﻿using System;
using System.Linq;
using cAlgo.API;
using cAlgo.API.Indicators;
using cAlgo.API.Internals;
using System.Collections.Generic;
using cAlgo.Indicators;
/*
notes:
if we keep opening trades without a limit, we are bound to have a trade on the lowest low or highest high.. we need to limit this with an indicator
*/
namespace cAlgo.Robots
{
    [Robot(TimeZone = TimeZones.UTC, AccessRights = AccessRights.FullAccess)]


    public class FXDynamicGrid : Robot
    {
        #region parameters


        [Parameter("Pip Size", DefaultValue = 10, MinValue = 1)]
        public int PipRadius { get; set; }

        [Parameter("take profit", DefaultValue = 10, MinValue = 0)]
        public double TakeProfit { get; set; }

        [Parameter("risk after pips", DefaultValue = 50, MinValue = 0)]
        public double RiskPips { get; set; }

        [Parameter("Volume", DefaultValue = 5000, MinValue = 1000)]
        public int Volume { get; set; }

        [Parameter("exponent increase volume", DefaultValue = true)]
        public bool volumeIncrease { get; set; }

        [Parameter("increase vol for winner", DefaultValue = 1.2, MinValue = 0, MaxValue = 9)]
        public double volIncrease { get; set; }

        [Parameter("Min AF", DefaultValue = 0.02, MinValue = 0, Group = "default")]
        public double minaf { get; set; }

        [Parameter("Max AF", DefaultValue = 0.2, MinValue = 0, Group = "default")]
        public double maxaf { get; set; }

        [Parameter("Buy", DefaultValue = true, Group = "default")]
        public bool Buy { get; set; }

        [Parameter("Sell", DefaultValue = true, Group = "default")]
        public bool Sell { get; set; }

        [Parameter("max spread", DefaultValue = 3, MinValue = 0, Group = "default")]
        public double MaxaSpread { get; set; }
        #endregion

        public API.Indicators.ParabolicSAR _parabolic { get; set; }
        #region variables
        public int buyWin, sellWin;

        private Grid buyGrid, sellGrid;
        public static double startBalance;
        private int calcVolume;
        public double maxDropDown;
        // start and end time is collected to see where the bot doesnt perform well and find a way to fix it.
        public DateTime startTime, endTime;
        #endregion

        protected override void OnStart()
        {
            // Put your initialization logic here
            _parabolic = Indicators.ParabolicSAR(minaf, maxaf);

            startBalance = Account.Balance;
            maxDropDown = 0;
            calcVolume = Volume * (int)(Account.Equity / startBalance);
            buyWin = 1;
            sellWin = 1;

            if (Buy && _parabolic.Result.LastValue < Symbol.Ask)
            {
                buyGrid = new Grid(TradeType.Buy, PipRadius, calcVolume, TakeProfit, RiskPips, this);
            }

            if (Sell && _parabolic.Result.LastValue > Symbol.Bid)
            {
                sellGrid = new Grid(TradeType.Sell, PipRadius, calcVolume, TakeProfit, RiskPips, this);
            }

            Positions.Closed += onClosePosition;
            startTime = Server.Time;
        }

        protected override void OnTick()
        {

            // Put your core logic here
            if (Symbol.Spread > (MaxaSpread * Symbol.PipSize))
                return;

            if (maxDropDown < Account.Balance - Account.Equity && Account.Balance > Account.Equity)
                maxDropDown = Account.Balance - Account.Equity;

            if (volumeIncrease)
            {
                calcVolume = Volume * (int)(Account.Equity / startBalance);
                if (calcVolume == 0)
                {
                    calcVolume = Volume;
                }
            }


            if (Buy && _parabolic.Result.LastValue < Symbol.Ask && buyGrid == null)
            {
                buyGrid = new Grid(TradeType.Buy, PipRadius, calcVolume, TakeProfit, RiskPips, this);
            }

            if (Sell && _parabolic.Result.LastValue > Symbol.Bid && sellGrid == null)
            {
                sellGrid = new Grid(TradeType.Sell, PipRadius, calcVolume, TakeProfit, RiskPips, this);
            }

            Grid.setState(buyGrid, sellGrid, Positions);

            if (Buy && buyGrid != null)
            {
                if (buyGrid.state.tradeCount == 0 && _parabolic.Result.LastValue < Symbol.Ask)
                {
                    int buyVol = calcVolume;
                    if (buyWin > 1)
                    {
                        buyVol = ((int)((((int)(volIncrease / 1)) + (volIncrease % 1) * buyWin) * buyVol) / 1000) * 1000;
                    }

                    buyGrid = new Grid(TradeType.Buy, PipRadius, buyVol, TakeProfit, RiskPips, this);
                }
                else
                {

                    buyGrid.matchFirstAndClose(Positions);
                    buyGrid.checkForNextGridLevel();

                }
            }
            if (Sell && sellGrid != null)
            {
                if (sellGrid.state.tradeCount == 0 && _parabolic.Result.LastValue > Symbol.Bid)
                {
                    int sellVol = calcVolume;
                    if (sellWin > 1)
                    {
                        sellVol = ((int)((((int)(volIncrease / 1)) + (volIncrease % 1) * sellWin) * sellVol) / 1000) * 1000;
                    }

                    sellGrid = new Grid(TradeType.Sell, PipRadius, sellVol, TakeProfit, RiskPips, this);
                }
                else
                {
                    sellGrid.matchFirstAndClose(Positions);
                    sellGrid.checkForNextGridLevel();
                }
            }
        }


        protected override void OnStop()
        {
            // Put your deinitialization logic here
            while (Positions.Count > 0)
            {
                Positions[0].Close();
            }
            String stat = "max losing down = " + maxDropDown + "\n" + "start balance: " + startBalance + "\n" + "end balance: " + Account.Balance + "\n" + "Buy " + Buy + "\n" + "Sell " + Sell + "\n" + "PipRadius " + PipRadius + "\n" + "TakeProfit " + TakeProfit + "\n" + "RiskPips " + RiskPips + "\n" + "Volume " + Volume + "\n" + "volumeIncrease " + volumeIncrease + "\n" + "volIncrease " + volIncrease + "\n" + "minaf " + minaf + "\n" + "maxaf " + maxaf + "\n" + "MaxaSpread " + MaxaSpread + "\n" + "currency pair " + Symbol.Name + "\n" + "start time " + startTime.ToString() + "\n" + "end time " + endTime.ToString() + "\n";


            long epochTicks = new DateTime(1970, 1, 1).Ticks;
            long unixTime = ((DateTime.UtcNow.Ticks - epochTicks) / TimeSpan.TicksPerSecond);
            Print("max losing down = {0}", maxDropDown);
            endTime = Server.Time;
            System.IO.File.WriteAllText(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "/Documents/cAlgo/Backtesting/FXDynamic test" + unixTime.ToString() + ".txt", stat);



        }

        protected void onClosePosition(PositionClosedEventArgs pos)
        {
            if (pos.Reason == PositionCloseReason.TakeProfit)
            {
                if (pos.Position.TradeType == TradeType.Buy)
                {

                    buyWin++;
                    sellWin = 1;
                }
                else
                {
                    sellWin++;
                    buyWin = 1;
                }
            }
            else
            {
                if (pos.Position.TradeType == TradeType.Buy)
                {
                    buyWin = 1;
                }
                else
                {
                    sellWin = 1;
                }
            }
        }
    }
    public class Grid
    {
        public TradeType gridType;
        public string gridId;
        public int pipRadius;
        public int volume;
        private Robot _myRobot;
        public State state;
        public double startBalance;
        public double takeProfit;
        public double riskPips;

        public Grid(TradeType gridType, int PipRadius, int calcVolume, double TakeProfit, double RiskPips, Robot robot)
        {
            _myRobot = robot;
            this.gridId = Guid.NewGuid().ToString();
            this.gridType = gridType;
            this.pipRadius = PipRadius;
            this.volume = calcVolume;
            this.takeProfit = TakeProfit;
            this.riskPips = RiskPips;
            TradeResult tr = _myRobot.ExecuteMarketOrder(gridType, _myRobot.Symbol.Name, volume, gridId, 0, takeProfit);
            state = new State();
            startBalance = _myRobot.Account.Balance;
        }

        public static void setState(Grid g1, Grid g2, Positions poss)
        {
            State.performSetLoop(g1, g2, poss);
        }

        public double closeGrid()
        {
            double total = 0;
            while (state.allOpenedPositions.Count > 0)
            {
                Position close = state.allOpenedPositions[0];
                total += close.NetProfit;
                close.Close();
            }
            return total;
        }

        public void matchFirstAndClose(Positions poss)
        {


            if (state.firstPosition == null)
                return;

            if (state.firstPosition.Pips >= takeProfit)
            {
                state.firstPosition.Close();
                return;
            }

            if (state.firstPosition.Pips <= -riskPips && (_myRobot.Account.Balance + state.firstPosition.NetProfit) >= startBalance)
            {
                //_myRobot.Print("difference {0} and startBalance {1}", (_myRobot.Account.Balance + state.firstPosition.NetProfit), startBalance);
                state.firstPosition.Close();
                return;
            }

            double firstProfit = state.firstPosition.NetProfit;

            if (state.tradeCount > 2 && state.firstPosition.NetProfit < 0)
            {

                double netPsitiveProfit = state.gridNetPositiveProfit;

                // first profit it a negative
                if (netPsitiveProfit + firstProfit >= 0)
                {


                    double totalClosed = firstProfit;

                    for (int i = state.allOpenedPositions.Count - 1; i > 0; i--)
                    {

                        if (state.allOpenedPositions[i].NetProfit > 0)
                        {

                            Position close = state.allOpenedPositions[i];
                            state.allOpenedPositions.Remove(close);
                            state.tradeCount--;
                            close.Close();
                            totalClosed += close.NetProfit;
                        }
                        if (totalClosed >= 0)
                        {

                            state.firstPosition.Close();
                            return;
                        }

                    }
                }
                else if (state.lowFirstTrade != null && state.firstPosition.Pips < -riskPips && netPsitiveProfit + state.lowFirstTrade.NetProfit >= 0 && state.lowFirstTrade.NetProfit < 0)
                {
                    double totalClosed = state.lowFirstTrade.NetProfit;

                    for (int i = state.allOpenedPositions.Count - 1; i > 0; i--)
                    {

                        if (state.allOpenedPositions[i].NetProfit > 0)
                        {

                            Position close = state.allOpenedPositions[i];
                            state.allOpenedPositions.Remove(close);
                            state.tradeCount--;
                            close.Close();
                            totalClosed += close.NetProfit;
                        }
                        if (totalClosed >= 0)
                        {

                            state.lowFirstTrade.Close();
                            return;
                        }
                    }
                }
                else if (state.lowFirstTrade == null || state.lowFirstTrade == state.lastPosition)
                {
                    if (state.lastPosition.Pips >= pipRadius)
                    {
                        state.lastPosition.Close();
                    }
                }

            }

        }

        public void checkForNextGridLevel()
        {
            // get last opened position

            if (state.lastPosition == null)
                return;
            if (gridType == TradeType.Buy)
            {

                if (_myRobot.Symbol.Ask <= state.lastPosition.EntryPrice - (_myRobot.Symbol.PipSize * pipRadius))
                {
                    TradeResult res = _myRobot.ExecuteMarketOrder(gridType, _myRobot.Symbol.Name, volume, gridId);
                }
            }
            else
            {
                if (_myRobot.Symbol.Bid >= state.lastPosition.EntryPrice + (_myRobot.Symbol.PipSize * pipRadius))
                {
                    TradeResult res = _myRobot.ExecuteMarketOrder(gridType, _myRobot.Symbol.Name, volume, gridId);
                }
            }
        }
    }

    public class State
    {
        public int tradeCount;
        public int buyCount;
        public int sellCount;
        public double gridNetProfit;
        public double gridNetPositiveProfit;
        public double gridNetNegativeProfit;
        public int numWinningTrades;
        public int numlosingTrades;
        public int numWinningSell;
        public int numLosingSell;
        public int numWinningBuy;
        public int numLosingBuy;
        public Position lowFirstTrade;
        public List<Position> allOpenedPositions;
        public Position firstPosition;
        public Position lastPosition;
        public State()
        {
            tradeCount = 0;
            buyCount = 0;
            sellCount = 0;
            gridNetProfit = 0;
            gridNetPositiveProfit = 0;
            gridNetNegativeProfit = 0;
            numWinningTrades = 0;
            numlosingTrades = 0;
            numWinningSell = 0;
            numLosingSell = 0;
            numWinningBuy = 0;
            numLosingBuy = 0;
            allOpenedPositions = new List<Position>();
            firstPosition = null;
            lastPosition = null;
            lowFirstTrade = null;
        }

        public static void performSetLoop(Grid grid1, Grid grid2, Positions positions)
        {
            if (grid1 != null)
            {
                grid1.state = new State();
            }
            if (grid2 != null)
            {
                grid2.state = new State();
            }
            Grid grid = null;
            for (int i = 0; i < positions.Count; i++)
            {
                Position current = positions[i];
                string gridId = "";
                grid = null;
                if (grid1 != null && current.Label == grid1.gridId)
                {
                    gridId = grid1.gridId;
                    grid = grid1;
                }
                else if (grid2 != null && current.Label == grid2.gridId)
                {
                    gridId = grid2.gridId;
                    grid = grid2;
                }

                if (grid != null)
                {
                    grid.state.allOpenedPositions.Add(current);
                    grid.state.tradeCount++;
                    grid.state.gridNetProfit += current.NetProfit;
                    grid.state.gridNetPositiveProfit += current.NetProfit > 0 ? current.NetProfit : 0;
                    grid.state.gridNetNegativeProfit += current.NetProfit < 0 ? current.NetProfit : 0;

                    grid.state.numWinningTrades += current.NetProfit > 0 ? 1 : 0;
                    grid.state.numlosingTrades += current.NetProfit < 0 ? 1 : 0;

                    if (current.TradeType == TradeType.Buy)
                    {
                        grid.state.buyCount++;
                        grid.state.numLosingBuy += current.NetProfit < 0 ? 1 : 0;
                        grid.state.numWinningBuy += current.NetProfit > 0 ? 1 : 0;

                    }
                    else
                    {
                        grid.state.sellCount++;
                        grid.state.numLosingSell += current.NetProfit < 0 ? 1 : 0;
                        grid.state.numWinningSell += current.NetProfit > 0 ? 1 : 0;
                    }
                    // set firstPosition
                    if (grid.state.firstPosition == null || grid.state.firstPosition.EntryTime > current.EntryTime)
                        grid.state.firstPosition = current;
                    // set last positions
                    if (grid.state.lastPosition == null || grid.state.lastPosition.EntryTime < current.EntryTime)
                        grid.state.lastPosition = current;
                    // set low first position
                    if ((grid.state.lowFirstTrade == null || grid.state.lowFirstTrade.EntryTime > current.EntryTime) && current.Pips > -grid.riskPips)
                        grid.state.lowFirstTrade = current;

                }
            }

        }
    }

}
