﻿using System;
using System.Linq;
using cAlgo.API;
using cAlgo.API.Indicators;
using cAlgo.API.Internals;
using cAlgo.Indicators;

namespace cAlgo.Robots
{
    [Robot(TimeZone = TimeZones.UTC, AccessRights = AccessRights.None)]
    public class randomTradeTaker : Robot
    {
        [Parameter(DefaultValue = 5.0)]
        public double radius { get; set; }

        Random rand = new Random();
        protected override void OnStart()
        {
            // Put your initialization logic here
        }

        protected override void OnTick()
        {
            // Put your core logic here
            if (Positions.Count == 0 || (Positions.Count == 1 && Positions[0].Pips < -5 * radius))
            {
                TradeType type = rand.NextDouble() > 0.5 ? TradeType.Buy : TradeType.Sell;
                ExecuteMarketOrder(type, Symbol.Name, 1000, "", radius * 30, radius);
            }
        }

        protected override void OnStop()
        {
            foreach (Position p in Positions)
            {
                p.Close();
            }
            // Put your deinitialization logic here
        }
    }
}
